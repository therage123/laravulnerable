FROM php:7.4.27

RUN apt update
RUN DEBIAN_FRONTEND="noninteractive" apt -y install tzdata libmcrypt-dev
RUN apt -y install supervisor

COPY app /app
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Install laravel deps
WORKDIR /app

EXPOSE 8000

CMD ["/usr/bin/supervisord"]