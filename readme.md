# Application Laravel vulnérable
## Disclaimer
Cette application n'est pas sécuritaire. Ne pas exécuter dans un environnement de production.

## Exécuter l'application
1. Clonez ce repo
```
$ git clone https://gitlab.com/sebast331-ctf/laravulnerable
```
2. Lancez l'application docker
```bash
$ cd laravulnerable
$ ./docker-run.sh
```
3. Naviguez vers http://localhost:8000

## Instructions pour la première connexion
1. Créez un compte l'aide du lien **Enregistrement**
    - Ce compte sera un administrateur
    - Le menu **Administration** n'est disponible que pour l'administrateur
1. Créez un second compte
    - Ce compte sera un utilisateur normal
    - Le menu **Administration** ne sera pas visible

## Défis (à partir d'un compte utilisateur standard)
- Se connecter en tant qu'administrateur (sans connaître le mot de passe)
- Changer son compte pour un compte admin sans voler son compte
- Visionner une liste privée d'un autre utilisateur
- Supprimer une liste privée d'un autre utilisateur
- Lire le fichier `/etc/passwd`
- Obtenir les noms d'utilisateurs et mots de passe de tous les usagers
- Lire le fichier `.env` et obtenir la variable APP_KEY pour chiffrer soi-même ses cookies
- Obtenir la totalité de la base de données

## Vulnérabilités
Les vulnérabilités sont disponibles dans le fichier [app/vulns.md](app/vulns.md)
