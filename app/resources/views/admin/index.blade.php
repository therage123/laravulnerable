@extends('layouts.base')

@section('titre')
    Utilisateurs
@endsection

@section('contenu')
        <table class="table table-hover align-baseline">
            <colgroup>
                <col style="width: 150px" />
            </colgroup>
            <thead>
                <tr>
                    <th scope="col">Nom</th>
                    <th scope="col">Courriel</th>
                    <th>Admin</th>
                </tr>
            </thead>
            <tbody>
            @foreach(\App\Models\User::all() as $user)
                <tr>
                    <th>{{ $user->name }}</th>
                    <td>{{ $user->email }}</td>
                    <td>
                        <form method="POST">
                            @csrf
                            <input type="hidden" name="userid" value="{{ $user->id }}" />
                            <input type="checkbox" name="admin" value="1" onclick="submit()" {{ $user->isAdmin() ? 'checked' : '' }} {{ $user == auth()->user() ? 'disabled' : '' }} >
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
@endsection
