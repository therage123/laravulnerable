@extends('layouts.base')

@section('titre')
    Rechercher
@endsection

@section('actions')
@endsection

@section('contenu')
    <div class="row">
        <form method="get">
            <div class="form-group">
                <label for="recherche">Entrez un terme afin de trouver des listes et des items</label>
                <input type="text" class="form-control" id="recherche" name="recherche" aria-describedby="recherche">
            </div>
            <div class="my-2">
                <button type="submit" class="btn btn-primary">Recherche</button>
            </div>
        </form>
    </div>

    <!-- Résultat de la recherche -->
    @isset($resultats)
        <hr>

        <h2>Résultat de recherche</h2>
        <h3 class="text-muted">{!! Request::get('recherche') !!}</h3>

        <table class="table table-hover align-baseline">
            <colgroup>
                <col style="width: 150px" />
            </colgroup>
            <thead>
            <tr>
                <th scope="col">Type</th>
                <th scope="col">Liste</th>
                <th scope="col">Nom</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach($resultats as $row)
                <tr>
                    <td>{{ $row->type }}</td>
                    <td>{{ $row->liste }}</td>
                    <td>{{ $row->nom }}</td>
                    <td>
                        <!-- Voir la liste -->
                        <a href="{{ route('listes.show', $row->id) }}" class="btn btn-secondary"><span class="material-icons">visibility</span></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endisset
@endsection
