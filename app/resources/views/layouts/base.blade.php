<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="{{ asset('css/base.css') }}" rel="stylesheet">

    <!-- Google Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <title>Application de listes</title>
</head>
<body>

<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand text-danger" href="{{ route('home') }}"><strong>Application Démo</strong></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('listes*') ? 'active' : '' }}" aria-current="page" href="{{ route('listes.index') }}">Mes listes ({{ Auth::user()->listes()->count() }})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('search*') ? 'active' : '' }}" aria-current="page" href="{{ route('search.index') }}">Rechercher</a>
                </li>
                @if (Auth::user()->isAdmin())
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin*') ? 'active' : '' }}" aria-current="page" href="{{ route('admin.index') }}">Administration</a>
                </li>
                @endif
            </ul>
            <ul class="navbar-nav me-2 mb-2 mb-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Bonjour {{ Auth::user()->name }}
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('logout') }}">Déconnexion</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">

    <div class="row justify-content-around">
        <div class="col">
            <h1>@yield('titre', 'Application de listes')</h1>
        </div>
        <div class="col align-self-center text-end">
            @yield('actions')
        </div>
    </div>


    @if(session('message'))
        <div class="alert alert-{{ session('couleur') }}" role="alert">
            {{ session('message') }}
        </div>
    @endif

    @yield('contenu')

</div>

<!-- Javascript -->
<script src="{{ asset('js/base.js') }}"></script>

</body>
</html>
