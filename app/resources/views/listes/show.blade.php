@extends('layouts.base')

@section('titre')
    {{ $liste->nom }}
@endsection

@section('contenu')
    <table class="table table-hover align-baseline">
        <thead>
        <tr>
            <th scope="col">Nom</th>
            <th scope="col">Quantité</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr>
                <th>{{ $item->nom }}</th>
                <td>{{ $item->quantite }}</td>
                <td>
                    <!-- Supprimer la liste -->
                    @if ($liste->user->id == Auth::user()->id)
                        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modal-delete-{{ $item->id }}">
                            <span class="material-icons">delete</span>
                        </button>
                        <div class="modal fade" id="modal-delete-{{ $item->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal-delete-{{ $item->id }}Label" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="modal-delete-{{ $item->id }}Label">Liste {{ $item->nom }}</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        Êtes-vous certain de vouloir supprimer <strong>{{ $item->nom }} ?</strong>
                                    </div>
                                    <div class="modal-footer">
                                        <form class="d-inline" method="post" action="{{ route('items.destroy', $item->id) }}">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger">Supprimer</button>
                                        </form>
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
