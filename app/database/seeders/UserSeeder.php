<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Basic users
        User::create([
            'name' => 'Alice',
            'email' => 'alice@email.com',
            'password' => bcrypt('Password1'),
            'plain_text_password' => 'Password1',
            'is_admin' => true,
        ]);
        User::create([
            'name' => 'Bob',
            'email' => 'bob@email.com',
            'password' => bcrypt('Password1'),
            'plain_text_password' => 'Password1'
        ]);
        User::create([
            'name' => 'Charlie',
            'email' => 'charlie@email.com',
            'password' => bcrypt('Password1'),
            'plain_text_password' => 'Password1'
        ]);
    }
}
