<?php

use App\Http\Controllers\ListesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/home', function() {
    return view('home');
})->middleware(['auth'])->name('home');

Route::resource('/listes', ListesController::class)
    ->middleware(['auth']);
Route::post('/listes/import', [ListesController::class, 'import'])
    ->name('listes.import')
    ->middleware(['auth']);

Route::get('/admin', [\App\Http\Controllers\AdminController::class, 'index'])
    ->name('admin.index')
    ->middleware(['auth']);
Route::post('/admin', [\App\Http\Controllers\AdminController::class, 'store'])
    ->name('admin.store')
    ->middleware(['auth']);

Route::resource('/search', \App\Http\Controllers\SearchController::class)
    ->middleware(['auth']);

Route::resource('/items', \App\Http\Controllers\ItemsController::class)
    ->middleware(['auth']);
