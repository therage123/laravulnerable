<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Liste;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ListesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Afficher les listes
        // - Mes listes
        // - Les listes publiques
        $mesListes = auth()->user()->listes()->with('user')->get();
        $pubListes = Liste::where('public', 1)->with('user')->get();
        $listes = $mesListes->merge($pubListes);

        return view('listes.index', [
            'listes' => $listes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Créer la liste
        $liste = new Liste();
        $liste->nom = $request->nomListe;
        $liste->public = ($request->listePublique === "1");
        $liste->User()->associate(auth()->user());
        $liste->save();

        // Rediriger
        return redirect()->route('listes.show', $liste);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Liste $liste)
    {
//        if (auth()->user()->user_id !== $liste->user_id) {
//            return abort(403, "Accès refusé");
//        }

        $items = $liste->items;
        return view('listes.show', [
            'liste' => $liste,
            'items' => $items
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Liste $liste)
    {
        $liste->delete();
        return redirect()->route('listes.index')
            ->with('message', 'La liste "' . $liste->nom . '" a été supprimée avec succès')
            ->with('couleur', 'success');
    }

    public function import(Request $request) {
        $fichierXml = $request->file('fichierXML');
        $xml = new \SimpleXMLElement($fichierXml->get(), LIBXML_NOENT);

        // Créer la liste
        $liste = new Liste();
        $liste->nom = (string) $xml->liste;
        $liste->public = false;
        $liste->User()->associate(auth()->user());
        $liste->save();

        // Ajouter les items
        foreach ($xml->items->item as $item) {
            $newItem = new Item();
            $newItem->nom = (string) $item;
            $newItem->quantite = (int) $item['qte'];
            $newItem->liste()->associate($liste);
            $newItem->save();
        }

        // Afficher la liste
        return back();
    }
}
