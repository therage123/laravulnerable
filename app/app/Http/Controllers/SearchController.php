<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Trouver les éléments
        if ($request->has('recherche')) {
            $listes = DB::table('listes')
                ->select('id', 'liste as type', 'nom as liste', 'nom')
                ->where('nom', 'like', DB::raw('"%' . $request->recherche . '%"'))
                ->where(function($query) {
                    $query->where('user_id', '=', auth()->user()->id)
                        ->orWhere('public', '=', 1);
                })
                ->get();

            $items = DB::table('items')
                ->select('listes.id as id', 'item as type', 'listes.nom as liste', 'items.nom')
                ->join('listes', 'listes.id', '=', 'items.liste_id')
                ->where('items.nom', 'like', DB::raw('"%' . $request->recherche . '%"'))
                ->where(function($query) {
                    $query->where('user_id', '=', auth()->user()->id)
                        ->orWhere('public', '=', 1);
                })
                ->get();

            $resultats = $listes->union($items);
            \Debugbar::info($resultats);

            return view('search.index', [
                'resultats' => $resultats
            ]);
        } else {
            // Afficher le formulaire de recherche
            return view('search.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
