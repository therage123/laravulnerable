<?php

namespace App\Http\Controllers;

use App\Models\User;
use DebugBar\DebugBar;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function index()
    {
        if (!auth()->user()->isAdmin()) {
            abort(403);
        }
        return view('admin.index');
    }

    public function store(Request $request)
    {
        if (!auth()->user()->isAdmin()) {
            abort(403);
        }

        $user = User::find($request->post('userid'));
        $user->is_admin = $request->post('admin') == 1;
        $user->save();
        return view('admin.index');
    }
}
